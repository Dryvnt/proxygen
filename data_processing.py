import jinja2
from enum import Enum
import re

definitely_broken_layouts = ['token', 'double_faced_token', 'vanguard', 'emblem', 'planar']
map_to_normal_layouts = ['meld', 'leveler', 'saga', 'augment', 'host', 'scheme']

def sanitize_cardname(cardname):
    fast_mapping = str.maketrans('àáâãéíöúûüŠ-', 'aaaaeiouuus ', '®,.\'"`’')
    slow_mapping = [
        ('Æ', 'ae'),  # legacy Æthersnipe and co.
        ('æ', 'ae'),  # see above
    ]
    out = cardname.strip()
    out = out.lower()
    out = out.translate(fast_mapping)
    for (o, n) in slow_mapping:
        out = out.replace(o, n)
    return out

def render(template_name, **template_vars):
    env = jinja2.Environment(
        loader=jinja2.FileSystemLoader('templates')
    )
    template = env.get_template(template_name)
    return template.render(**template_vars)

class CardType(Enum):
    UNSUPPORTED = 0 #not broken but also not supported
    CREATURE = 1
    PLANESWALKER = 2
    NONCREATURE = 3
    TRANSFORM = 4
    SPLIT = 5
    FLIP = 6


class RawCard:
    def __init__(self, json_entry):
        self.type = CardType.UNSUPPORTED
        self.pretty_name = json_entry['name']
        self.name = sanitize_cardname(json_entry['name'])
        self.scryfall_uri=json_entry['scryfall_uri']
        self.oracle_text = json_entry.get('oracle_text', '')
        self.typeline = json_entry['type_line']
        self.manacost = json_entry.get('mana_cost', '')
        self.manacost_html = self.manacost.replace('}{', '}<wbr>{')

        self.layout = json_entry['layout']

        if self.layout in map_to_normal_layouts:
            self.layout = 'normal'

        if self.layout == 'normal':
            if 'Creature' in self.typeline or 'Vehicle' in self.typeline:
                self.type = CardType.CREATURE
            elif 'Planeswalker' in self.typeline:
                self.type = CardType.PLANESWALKER
            else:
                self.type = CardType.NONCREATURE

            self.power = json_entry.get('power', '')
            self.toughness = json_entry.get('toughness', '')
            self.loyalty = json_entry.get('loyalty', '')
        elif self.layout in ['transform', 'split', 'flip']:
            if self.layout == 'transform':
                self.type = CardType.TRANSFORM
            elif self.layout == 'split':
                self.type = CardType.SPLIT
            elif self.layout == 'flip':
                self.type = CardType.FLIP
            faces = json_entry['card_faces']
            assert(len(faces) > 0)
            self.faces = []
            for face in faces:
                fake_entry = {
                    'layout': 'normal',
                    'name': face['name'],
                    'scryfall_uri': self.scryfall_uri,
                    'mana_cost': face['mana_cost'],
                    'type_line': face['type_line'],
                    'oracle_text': face.get('oracle_text', ''),
                    'power': face.get('power', ''),
                    'toughness': face.get('toughness', ''),
                    'loyalty': face.get('loyalty', ''),
                }
                self.faces.append(RawCard(fake_entry))
            self.name = self.faces[0].name
        else:
            return None
        self.parse_oracle()

    def parse_oracle(self):
        if self.layout == 'normal': 
            if self.type == CardType.UNSUPPORTED:
                self.oracle_text += f'This type of card ({self.layout}) is not officially supported, but might work.\nGo complain to the developer.'
            self.oracle_lines = []
            for line in self.oracle_text.splitlines():
                line = line.replace('(', '<i>(').replace(')', ')</i>')
                self.oracle_lines.append(line)

    def inner_html(self):
        assert(self.type in [CardType.CREATURE, CardType.PLANESWALKER,
                             CardType.NONCREATURE, CardType.UNSUPPORTED])
        if self.type == CardType.CREATURE:
            return render('creature_inner.html', card=self)
        if self.type == CardType.PLANESWALKER:
            return render('planeswalker_inner.html', card=self)
        if self.type == CardType.NONCREATURE:
            return render('noncreature_inner.html', card=self)
        if self.type == CardType.UNSUPPORTED:
            return render('noncreature_inner.html', card=self)

    def to_html(self):
        if self.layout == 'normal':
            return render('layout_normal.html', card=self)
        if self.layout == 'transform':
            return render('layout_transform.html', card=self)
        if self.layout == 'split':
            return render('layout_split.html', card=self)
        if self.layout == 'flip':
            return render('layout_flip.html', card=self)
        # fallback for unsupported
        return render('layout_normal.html', card=self)

def rawcard_from_json(json_entry):
    if json_entry['layout'] in definitely_broken_layouts:
        return None
    return RawCard(json_entry)
